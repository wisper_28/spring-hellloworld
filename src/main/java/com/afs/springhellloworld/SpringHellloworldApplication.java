package com.afs.springhellloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHellloworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringHellloworldApplication.class, args);
	}

}
