package com.afs.springhellloworld.Controller;

import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/hello")
public class HelloController {
    @GetMapping
    public String hello() {
        return "hello";
    }

    @GetMapping("/{name}")
    public String helloWorld(@PathVariable String name) {
        return "Hello " + name;
    }

    @GetMapping(params = {"language"})
    public String helloByLanguage(@RequestParam String language) {
        return Objects.equals(language, "cn")? "你好" : "hello";
    }
}
